# frozen_string_literal: true

require_relative 'lib/eslint-webpacker/version'

Gem::Specification.new do |spec|
  spec.name          = 'eslint-webpacker'
  spec.version       = ESLintWebpacker::VERSION
  spec.authors       = ['Guillaume Hain']
  spec.email         = ['zedtux@zedroot.org']
  spec.summary       = 'Runs eslint or eslint_d from Ruby'
  spec.description   = 'This gem is useful when added to the Rakefile in ' \
                       'order to scan each of your JavaScript files in a ' \
                       'Ruby On Rails project using the webpacker gem.'
  spec.homepage      = 'https://gitlab.com/zedtux/eslint-webpacker'
  spec.license       = 'MIT'

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}-/blob/master/CHANGELOG.md"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 2.4'

  spec.add_dependency 'colorize'
  spec.add_dependency 'railties', '>= 3.2'
  spec.add_dependency 'webpacker'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
end
