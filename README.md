# eslint-webpacker

A Ruby on Rails gem that runs `eslint` (https://eslint.org/) from the `node_modules` folder built with [the webpacker gem](https://github.com/rails/webpacker).

I've made this gem in order to use it in my project's `Rakefile` so that it checks the javascript files, like [rubocop](https://github.com/rubocop/rubocop) does for Ruby files.

Supported file extensions are:

- _.js_
- _.jsx_
- _.es6_

This repo is a fork of [the eslint-rails gem](https://github.com/appfolio/eslint-rails).

## Installation

Add this line to your application's Gemfile:

```ruby
group :development do
  gem 'eslint-rails'
end
```

If not already done, install the `eslint` package and configure your plugins. Also see bellow if it's too slow.

## Usage

### CLI

This gem comes with one Rake task:

```sh
rake eslint:run
```

Ran like this, it will scan all the JavaScript files from your project.

But you can supply a filename to the task, using several different formats, and it will lint just that file. For example, to analyze `app/javascript/components/utilities.js`, you can run any of the following:

```sh
rake eslint:run[components/utilities]
rake eslint:run[components/utilities.js]
rake eslint:run[app/javascript/components/utilities]
rake eslint:run[app/javascript/components/utilities.js]
```

## But it is really slow!

You are using the `eslint` NPM package which requires to boot Node JS for each files.

In order to speed up the checks, install [estlint_d](https://github.com/mantoni/eslint_d.js) which runs ESlint in a daemon :

```
yarn add eslint_d
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/zedtux/eslint-webpacker.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
