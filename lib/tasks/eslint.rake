# frozen_string_literal: true

require 'eslint-webpacker'

namespace :eslint do
  def run_and_print_results(file)
    puts 'Running Eslint...'
    warnings = ESLintWebpacker::Runner.new(file).run

    return if warnings.empty?

    formatter = ESLintWebpacker::TextFormatter.new(warnings)
    formatter.format
    exit 1
  end

  desc 'Run ESLint against the specified JavaScript file or the entire ' \
       'project and report warnings'
  task :run, [:filename] => :environment do |_, args|
    run_and_print_results(args[:filename])
  end
end
