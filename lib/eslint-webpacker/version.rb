# frozen_string_literal: true

module ESLintWebpacker
  VERSION = '1.1.0'
end
