# frozen_string_literal: true

require 'colorize'

module ESLintWebpacker
  class Runner
    include ActionView::Helpers::JavaScriptHelper

    JAVASCRIPT_EXTENSIONS = %w[.js .jsx .es6].freeze

    def initialize(file)
      @file = normalize_infile(file)
    end

    def run
      javascripts = javascript_files

      puts "Inspecting #{javascripts.size} files"

      warnings = javascripts.map do |javascript|
        generate_warnings(javascript).tap { |warns| output_progress(warns) }
      end

      puts

      warnings.flatten
    end

    private

    def descendant?(file_a, file_b)
      a_list = file_a.to_s.split('/')
      b_list = file_b.to_s.split('/')

      b_list[0..a_list.size - 1] == a_list
    end

    def eslint_d_available?
      File.exist?(Rails.root.join('node_modules', '.bin', 'eslint_d'))
    end

    def generate_warnings(javascript)
      relative_path = javascript.relative_path_from(Pathname.new(Dir.pwd))

      warning_hashes(javascript).flat_map do |hash|
        next if hash['errorCount'].zero?

        hash['messages'].map do |message|
          ESLintWebpacker::Warning.new(relative_path, message)
        end
      end.compact
    end

    def javascript_files
      all_javascripts = webpacker_files.select do |asset|
        JAVASCRIPT_EXTENSIONS.include?(asset.extname)
      end

      javascript_files = all_javascripts.select { |a| descendant?(@file, a) }

      javascript_files.reject do |path|
        path.to_s =~ /eslint.js|vendor|gems|min.js|editorial/
      end
    end

    def normalize_infile(file)
      # Remove beginning of asset path
      file = file.to_s.gsub(%r{^app/javascript/}, '')

      # Ensure path is absolute
      file = Pathname.new("#{Dir.pwd}/app/javascript/#{file}")

      return file if file.directory?
      return file if file.extname.present?

      # Make sure it has an extension
      Pathname.new("#{file}.js")
    end

    def output_progress(warnings)
      print case file_severity(warnings)
            when :high
              'H'.red
            when :low
              'L'.yellow
            else
              '.'.green
            end
    end

    def warning_hashes(file_path)
      command = eslint_d_available? ? 'eslint_d' : 'eslint'

      JSON.parse(`./node_modules/.bin/#{command} --format json #{file_path}`)
    end

    def webpacker_files
      Dir.glob(Rails.root.join('app', 'javascript', '**/*')).map do |path|
        Pathname.new(path)
      end
    end

    def file_severity(warnings)
      return :none if warnings.blank?

      warnings.map(&:severity).uniq.min
    end
  end
end
